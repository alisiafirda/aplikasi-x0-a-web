<?php
$DB_NAME = "kampus";
    $DB_USER = "root";
    $DB_PASS = "";
    $DB_SERVER_LOC = "localhost";

    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $sql = "SELECT m.nim, m.nama, m.jns_kelamin, m.alamat, p.nama_prodi, m.photos
            FROM mahasiswa m, prodi p 
            WHERE m.id_prodi = p.id_prodi";
    
    $result = mysqli_query($conn,$sql);
?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Mahasiswa</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-primary">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="mahasiswa.php">Mahasiswa</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="prodi.php">Prodi</a>
                </li>
            </ul>
        </div>
    </nav>
    <br>
    <div class="container">
        <h1>Data Mahasiswa</h1>
        <div class="table-responsive">
            <table class="table table-bordered table-striped bg-light">
                <thead class="table-striped table-primary" style="text-align: center;">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">NIM</th>
                        <th scope="col">Nama Mahasiswa</th>
                        <th scope="col">Jenis Kelamin</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Prodi</th>
                        <th scope="col">Photo</th>
                    </tr>
                </thead>
                <?php
                    $no=1;
                    while($mhs = mysqli_fetch_assoc($result)){
                ?>
                <tbody>
                    <tr>
                        <th scope="row" style="text-align: center;"><?php echo $no;$no++; ?></th>
                        <td><?php echo $mhs['nim']; ?></td>
                        <td><?php echo $mhs['nama']; ?></td>
                        <td><?php echo $mhs['jns_kelamin']; ?></td>
                        <td><?php echo $mhs['alamat']; ?></td>
                        <td><?php echo $mhs['nama_prodi']; ?></td>
                        <td style="text-align: center;">
                            <img src="images/<?php echo $mhs['photos']; ?>" style="width: 200px;" alt="Foto Mahasiswa">
                        </td>
                    </tr>
                    </tr>
                </tbody>
                <?php } ?>
            </table>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
</body>

</html>